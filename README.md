# Read me

This document describes the different steps to do in order to import the extract PDF application to a Linux machine

## Postgres Setup

- Install postgres on your machine using the following command:

  ```bash
  sudo apt install postgresql
  ```

- Login in as postgres user

  ```bash
  sudo -i -u postgres
  ```

- Launch postgres database

  ```bash
  psql
  ```

- Create a new user

  ```sql
  create role sfadel login;
  ```

- Give the user privilege to create a new database

  ```sql
  Alter role sfadel createdb;
  ```

- Create the database

  ```sql
  Create database pdfextractdb owner sfadel;
  ```

- Give the user a password to connect to the database

  ```sql
  Alter role sfadel with encrypted password 'welcome1';
  ```

- Now exit 2 times to get back to the machine user. We have finished setting up the Postgres Database



## Import Postgres schema

- Import the pdf_extractor_project.zip file to your machine

- Unzip the file using the command:

  ```bash
  unzip pdf_extractor_project.zip
  ```

- go to the directory pdf_extractor_project

- Run the following command to import the schema of the postgres database

  ```bash
  psql pdfextractdb < pdfextractdb.dump
  ```

Now you have the database with its schema setup on your machine



## Application source code



- install the following packages:

  ```bash
  sudo apt install python3-pip
  sudo apt install python3-flask
  ```

- Go to the directory "pdfextractor/pdfextractor/"

  ```
  cd pdfextractor/pdfextractor/
  ```

- run the following command to install all pip packages

  ```bash
  pip install -r requirements.txt
  ```

- and now you can test the application

  ```bash
  FLASK_APP=view.py flask run
  ```

  You can open the flask application on the browser, upload a file, view its metadata and content. Hyperlink exist at the bottom of the page to be able to go to the directory needed (upload a new file, go to content, go to metadata)



## Test

- Install pytest-3 package

```bash
sudo apt install python3-pytest
```

-  go to the directory pdfextract/tests and run the following command:

```bash
pytest-3
```

all tests should pass

- we can see the coverage of our code using the command:

```bash
python3 -m pytest --cov=../pdfextractor/ --cov-report html
```

a new folder "htmlcov" will b created in the tests directory. in this folder open the "index.html" and there we can see the coverage of each py file. we have a 78% coverage in total. (P.S. you can find the html file in the zip file)



## Pylint

W have used pylint to ensure we have no warning. if you don't have pylint installed:

```bash
sudo apt install pylint
```

Please run the following command in the directory /pdfextractor/pdfextractor

```bash
pylint .
```

we can see that the code is rated 10/10