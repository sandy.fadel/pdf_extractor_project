import psycopg2
from psycopg2 import Error
from decouple import config
from flask import render_template

class PDFDetails:
    def __init__(self,doctitle,docauthor,createddate,
                    modifieddate,doctext,numpages,pdffile):
        self.doctitle = doctitle
        self.docauthor = docauthor
        self.createddate = createddate
        self.modifieddate = modifieddate
        self.doctext = doctext
        self.numpages = numpages
        self.pdffile = pdffile
        # Connect to an existing database
        # The userId and password are being read from the .env file
        self._connection = psycopg2.connect(
            user=config("userID", default=""),
            password=config("password", default=""),
            host="127.0.0.1",
            port="5432",
            database="pdfextractdb",
        )

        # Create a cursor to perform database operations
        self._cursor = self._connection.cursor()

    def insert_pdf(self):
        try:
            # Executing an insert SQL query
            self._cursor.execute("select last_value from doc_sn;")
            doc_id = self._cursor.fetchone()
            query_tuple = (
                self.doctitle,
                self.docauthor,
                self.createddate,
                self.modifieddate,
                self.doctext,
                self.numpages,
                self.pdffile,
            )
            insert_query = "INSERT INTO pdf_details values(nextval('doc_sn'),%s,%s,%s,%s,%s,%s,%s);"
            self._cursor.execute(insert_query, query_tuple)
            self._connection.commit()
            print(doc_id[0])
            return doc_id[0]

        except (Exception, Error) as error:
            print("Error while connecting to PostgreSQL" , error)
            return "Error while connecting to PostgreSQL"
        finally:
            if self._connection:
                self._cursor.close()
                self._connection.close()
                print("PostgreSQL connection is closed")

#here the insert is being done and then we render the template
    def template_insert(self):
        docid=self.insert_pdf()
        return render_template(
                "get_meta.html", doc_id=docid
                )

#here we are getting the metadata and content from the database
    def get_pdf(self, doc_id, return_type):
        try:
            # Executing an insert SQL query
            print(doc_id)
            if return_type == "metadata":
                get_query = """select title,author,created_date,
                                modified_date,num_pages from pdf_details where doc_id = {};"""
            elif return_type == "content":
                get_query = "select doc_text from pdf_details where doc_id = {};"
            self._cursor.execute(get_query.format(doc_id))
            meta = self._cursor.fetchall()
            if return_type == "metadata":
                return render_template("get_meta_data.html", rows=meta[0], docid=doc_id)
            return render_template("get_context.html", rows=meta[0], docid=doc_id)

        except (Exception, Error) as error:
            print("Error while connecting to PostgreSQL"  , error)
            if str(error) == "list index out of range":
                return "Document doesn't exist in the Database. Please choose another id"
            return "Error while connecting to PostgreSQL"
        finally:
            if self._connection:
                self._cursor.close()
                self._connection.close()
                print("PostgreSQL connection is closed")
