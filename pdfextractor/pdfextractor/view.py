from pdfextractor.controller.controller import ApiController
from pdfextractor import app

#go to the upload page
@app.route("/upload")
def upload_file():
    return ApiController.upload_file()

#go to controller to extract the text from pdf
@app.route("/documents", methods=["GET", "POST"])
def uploader_file():
    return ApiController.uploader_file()

#display document metadata
@app.route("/documents/<doc_id>", methods=["GET"])
def get_meta(doc_id):
    return ApiController.get_file(doc_id, "metadata")

#display document content
@app.route("/documents/<doc_id>.txt", methods=["GET"])
def get_cont(doc_id):
    return ApiController.get_file(doc_id, "content")


if __name__ == "__main__":
    app.run(debug=True)
