import base64
import datetime
import pdfminer.high_level
from flask import render_template, request
from pdfextractor.model.pdf_details import PDFDetails
from PyPDF2 import PdfFileReader


class ApiController:
    def __init__(self: object):
        pass

#check if uploaded file is a pdf or not
    @staticmethod
    def allowed_file(f_name):
        doc_type = f_name[int((f_name).rfind(".")) + 1 : int(len(f_name))]
        if doc_type.lower() != "pdf":
            return "Invalid Document"
        return "Valid Document"


    @staticmethod
    def upload_file():
        return render_template("upload.html")

#here is the extraction of the text being happened
    @staticmethod
    def uploader_file():
        if request.method == "POST":
            fil = request.files["file"]
            f_name = fil.filename
            if ApiController.allowed_file(f_name) == "Invalid Document":
                return "Invalid Document"
            fil.save("PDF/" + fil.filename)
            with open("PDF/" + str(fil.filename), "rb") as pdf_file:
                base64_encoded_data = base64.b64encode(pdf_file.read())
                base64_message = base64_encoded_data.decode("utf-8")

                txt = pdfminer.high_level.extract_text(pdf_file)
                pdfreader = PdfFileReader(pdf_file)
                num_pag = pdfreader.getNumPages()
                data = pdfreader.getDocumentInfo()
                author = data.author
                title = data.title
                #changing date format
                if "CreationDate" in str(pdfreader.documentInfo):
                    created_date = datetime.datetime.strptime(
                            pdfreader.documentInfo["/CreationDate"][0:15], "D:%Y%m%d%H%M%S"
                            ).strftime("%Y-%m-%d %H:%M:%S")
                else:
                    created_date=None
                if "ModDate" in str(pdfreader.documentInfo):
                    moddate = datetime.datetime.strptime(
                            pdfreader.documentInfo["/ModDate"][0:15], "D:%Y%m%d%H%M%S"
                            ).strftime("%Y-%m-%d %H:%M:%S")
                else:
                    moddate=None
                pdfobj = PDFDetails(
                    title, author, created_date, moddate, txt, num_pag, base64_message
                )
            return pdfobj.template_insert()
        return "Please insert the document id as follow: /documents/doc_id"

#here is the method where we get the metadata and content of text depending on return type
    @staticmethod
    def get_file(doc_id, return_type):
        pdfobj = PDFDetails("", "", "", "", "", "", "")
        return pdfobj.get_pdf(doc_id, return_type)
