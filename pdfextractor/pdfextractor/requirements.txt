pdfminer.six==20211012
pypdf2==1.26.0
Flask==2.0.2
psycopg2-binary==2.9.3
python-decouple==3.5
pytest-cov==3.0.0
