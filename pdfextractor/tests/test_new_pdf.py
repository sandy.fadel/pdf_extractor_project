from pdfextractor.model.pdf_details import PDFDetails

def test_new_pdf():
    pdf = PDFDetails("Variational Probabilistic Multi-Hypothesis Tracking"
            ,"Shuoyuan Xu, Hyo-Sang Shin and Antonios Tsourdos;"
            ,"",""
            , "","23","")
    assert pdf.doctitle == "Variational Probabilistic Multi-Hypothesis Tracking"
    assert pdf.docauthor == "Shuoyuan Xu, Hyo-Sang Shin and Antonios Tsourdos;"
    assert pdf.numpages == "23"
