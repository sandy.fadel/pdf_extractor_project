from pdfextractor.controller.controller import ApiController

def test_nbr_arg():
    assert ApiController.get_file("10000000000000000","metadata") == "Document doesn't"\
            " exist in the Database. Please choose another id"
