from pdfextractor.controller.controller import ApiController

def test_pdf_type():
    with open("2110.14646.pdf", "r", encoding='utf8') as fil:
        assert ApiController.allowed_file(fil.name)=="Valid Document"

    with open("article.txt", "r", encoding='utf8') as txt:
        assert ApiController.allowed_file(txt.name)=="Invalid Document"
