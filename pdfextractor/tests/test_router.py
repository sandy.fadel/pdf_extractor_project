from datetime import date
import pytest
import pdfextractor.model.pdf_details as PDF_det
from pdfextractor.view import app

@pytest.fixture
def insert_pdf():
    pdfobj=PDF_det.PDFDetails("testtitle","testauthor",date.today(),date.today(),"testtext","23","")
    docid=pdfobj.insert_pdf()
    print("docid " + str(docid))
    return docid


def test_router(insert_pdf):
    with app.test_client() as test_client:
        response = test_client.get("/upload")
        assert response.status_code == 200

        response = test_client.get("/documents")
        assert response.status_code == 200

        response = test_client.get("/documents/" + str(insert_pdf))
        assert response.status_code == 200

        response = test_client.get("/documents/")
        assert response.status_code == 404

        response = test_client.get("/documents/" + str(insert_pdf) + ".txt")
        assert response.status_code == 200
